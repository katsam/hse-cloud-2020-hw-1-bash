#!/bin/bash

readarray -t countries
len=${#countries[@]}
for ((i=0; i<$len; i++))
do
	elem=${countries[i]}
	if [[ ${elem} != ${elem,} ]]
	then
		n=${#elem}
		countries[i]=".${elem:1}"
	fi
done

echo ${countries[@]}

