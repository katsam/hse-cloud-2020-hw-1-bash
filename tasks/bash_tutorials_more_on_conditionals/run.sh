#!/bin/bash

read X
read Y
read Z

if [[ ($X == $Y) && ($Y == $Z) && ($Z == $X) ]]; then
	echo "EQUILATERAL"
elif [[ (($X == $Y) && ($X != $Z)) || (($Y == $Z) && ($Y != $X)) || (($Z == $X) && ($Z != $Y)) ]]; then
	echo "ISOSCELES"
else
	echo "SCALENE"
fi
