#!/bin/bash

readarray -t countries
declare -a without_a
for elem in "${countries[@]}"
do
	if [[ $elem != *"a"* ]]
	then
		if [[ $elem != *"A"* ]]
		then
			without_a+=( "$elem" )
		fi
	fi
done
echo ${without_a[@]}
