#!/bin/bash

awk '
{
n = split($0, array, " ")
sum_scores = array[2] + array[3] + array[4]
grade = "FAIL"
if (sum_scores >= 80*3) grade="A"
else
{
  if (sum_scores >= 60*3) grade="B"
}

print $0 " : " grade
}'

