#!/bin/bash

awk '
{
n=split($0, array, " ")
if (n < 4) print "Not all scores are available for " array[1]
}
'
